// console.log("Hello");



// [SECTION]  - While Loop

let count = 0;

// While the value of count isnot equal to 0
while(count !== 5){
	// First iteration  -> count == 5
	// 2nd iteration  -> count == 4
	// 3rd iteration  -> count == 3
	// 4th iteration  -> count == 2
	// 5th iteration  -> count == 1
	console.log("While: " + count);
	count++;
}



// [SECTION]  - Do While Loop
/*
Syntax:
Do{
	//code block
}while(expression/condition)
*/

								/*let number = Number(prompt("Give me a number"));
								do{
									// The current value of number is printed out.
									console.log("Do while " + number)
									// Increases the value of number by 1 after every iteration to stop the loop when it reaches to 10
									number +=1;
								}while(number < 10)*/


// [SECTION]  - For Loop
/*
Syntax:
for(initialization; condition; finalExpression/iteration){
	//code blocks
}
*/

								/*for(let count = 0; count <= 20; count++){
									console.log("For loop: " + count);
								}*/


// let myString = "Alex";
// // Characters in strong may be counted using the .legnth property
// // Strings are specials compared to other data types in that access to dunctions and other pieces of information
// console.log(myString.length)

// // accessing elements of a string
// // individual characters

// console.log(myString[0]);
// console.log(myString[1]);
// console.log(myString[2]);
// console.log(myString[3]);

// Will create loop that will print out the individual letters of my string variables.
							/*for(let x =0; x < myString.length; x++){
								// THe current value of my dtring is printed out using its index
								console.log(myString[x]);
							}*/


// Create a string with names "myName"
// let myName = "ALEx";

// for(let i = 0; i < myName.length; i++){
// 	// console.log(myName[i].toLowerCase());
// 	if(
// 		myName[i].toLowerCase() == "a" ||
// 		myName[i].toLowerCase() == "i" ||
// 		myName[i].toLowerCase() == "o" ||
// 		myName[i].toLowerCase() == "u" ||
// 		myName[i].toLowerCase() == "e" 
// 		){
// 		// If the letter in the name is a vowel, it will print number 3
// 		console.log(3);
// 	}else{
// 		// print in the console or non vowel characters
// 		console.log(myName[i])
// 	}
// }


// [SRCTION] - Continue and Break Statements


					// for(let count = 0; count <=10; count++){
					// 	// If remainder is equal to 0 
					// 	if(count % 2 === 0){
					// 		continue;
					// 	}

					// 	console.log("Continue and Break " + count);

						//If the current value of count is greater than 10 the flow will stop
						// if (count > 10){
						// 	break;
						// }
					// }



let name = "alexandro"

for(let i = 0; i < name.length; i++){
	// Will print the current letters base on its index
	console.log(name[i]);


	// if the vowel is equal  to a, continue the next iteration of the loop
	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}
}